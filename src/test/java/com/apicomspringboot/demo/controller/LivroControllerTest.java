package com.apicomspringboot.demo.controller;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.standaloneSetup;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.*;

import com.apicomspringboot.demo.controller.v1.LivroController;
import com.apicomspringboot.demo.models.Livro;
import com.apicomspringboot.demo.service.LivroService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import io.restassured.http.ContentType;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest
public class LivroControllerTest {
    @Autowired
    private LivroController livroController;

    @MockBean
    private LivroService livroService;

    @BeforeEach
    public void setup() {
        standaloneSetup(this.livroController);
    }

    @Test
    public void deveRetornarSucesso_QuandoBuscarLivro() {
        Livro livro1 = new Livro();
        livro1.setLivroId(1L);
        livro1.setTitulo("Livro 1");
        livro1.setIsbn("Teste");
        livro1.setAutor("Teste Autor 1");
        livro1.setEdicao(1);
        livro1.setEditora("Livro 1 Teste");
        List<Livro> lista = new ArrayList<>();
        lista.add(livro1);

        when(this.livroService.findById(1L)).thenReturn(livro1);
        given()
                .accept(ContentType.JSON)
                .when()
                .get("/v1/livros/{livroId}", livro1.getLivroId())
                .then()
                .statusCode(HttpStatus.OK.value());
    }

    @Test
    public void deveRetornarSucesso_QuandoBuscarAListaDeLivros() {
//        Livro livro1 = new Livro(1L, "Livro1", "Teste", "Teste", "Teste", 1);
        Livro livro1 = new Livro();
        livro1.setLivroId(1L);
        livro1.setTitulo("Livro 1");
        livro1.setIsbn("Teste");
        livro1.setAutor("Teste Autor 1");
        livro1.setEdicao(1);
        livro1.setEditora("Livro 1 Teste");
        List<Livro> lista = new ArrayList<>();
        lista.add(livro1);

        when(this.livroService.select()).thenReturn(lista);
        given()
                .accept(ContentType.JSON)
                .when()
                .get("/v1/livros/")
                .then()
                .statusCode(HttpStatus.OK.value());
    }

    @Test
    public void deveRetornarNaoEncontrado_QuandoBuscarLivro() {
//        Livro livro1 = new Livro(1L, "Livro1", "Teste", "Teste", "Teste", 1);
        Livro livro1 = new Livro();
        livro1.setLivroId(1L);
        livro1.setTitulo("Livro 1");
        livro1.setIsbn("Teste");
        livro1.setAutor("Teste Autor 1");
        livro1.setEdicao(1);
        livro1.setEditora("Livro 1 Teste");
        List<Livro> lista = new ArrayList<>();
        lista.add(livro1);

        when(this.livroService.findById(5L))
                .thenReturn(null);

        given()
                .accept(ContentType.JSON)
                .when()
                .get("/livros/{codigo}", 5L)
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void deveSalvarComSucesso_UmLivro() {
//        Livro livro1 = new Livro(1L, "Livro1", "Teste", "Teste", "Teste", 1);
        Livro livro1 = new Livro();
        livro1.setLivroId(1L);
        livro1.setTitulo("Livro 1");
        livro1.setIsbn("Teste");
        livro1.setAutor("Teste Autor 1");
        livro1.setEdicao(1);
        livro1.setEditora("Livro 1 Teste");

        this.livroService.save(livro1);
        Assertions.assertThat(livro1.getLivroId()).isNotNull();
        Assertions.assertThat(livro1.getTitulo()).isEqualTo("Livro 1");
    }

    @Test
    public void deveDeletarComSucesso_UmLivro() {
//        Livro livro1 = new Livro(1L, "Livro1", "Teste", "Teste", "Teste", 1);
        Livro livro1 = new Livro();
        livro1.setLivroId(1L);
        livro1.setTitulo("Livro 1");
        livro1.setIsbn("Teste");
        livro1.setAutor("Teste Autor 1");
        livro1.setEdicao(1);
        livro1.setEditora("Livro 1 Teste");
        this.livroService.save(livro1);
        livroService.delete(1L);
        assertThat(livroService.findById(livro1.getLivroId())).isNull();

    }

    @Test
    public void deveFazerUpdateComSucesso_UmLivro() {
//        Livro livro1 = new Livro(1L, "Livro1", "Teste", "Teste", "Teste", 1);
        Livro livro1 = new Livro();
        livro1.setLivroId(1L);
        livro1.setTitulo("Livro 1");
        livro1.setIsbn("Teste");
        livro1.setAutor("Teste Autor 1");
        livro1.setEdicao(1);
        livro1.setEditora("Livro 1 Teste");

        this.livroService.save(livro1);
        livro1.setTitulo("Livro Update");
        livro1.setAutor("Teste Update");
        this.livroService.update(livro1);
        Assertions.assertThat(livro1.getTitulo()).isEqualTo("Livro Update");

    }
}
