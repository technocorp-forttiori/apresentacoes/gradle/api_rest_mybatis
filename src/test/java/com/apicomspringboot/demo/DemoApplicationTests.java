package com.apicomspringboot.demo;

import com.apicomspringboot.demo.controller.v1.LivroController;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

//WebMvcTest -> Sobe o contexto da camada de Web.
//AutoConfigureMvc -> Sobe todo o contexto da aplicação.

@RunWith(SpringRunner.class)
//@SpringBootTest
@WebMvcTest(controllers = LivroController.class)
class DemoApplicationTests {

	@Autowired
	private MockMvc mockMvc; // principal ponto de entrada para o controller

	@Test
	void testeGetEndPoint() throws Exception {
	}

}
