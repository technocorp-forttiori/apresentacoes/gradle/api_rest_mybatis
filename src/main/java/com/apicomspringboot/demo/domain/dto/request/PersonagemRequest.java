package com.apicomspringboot.demo.domain.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Table(name = "personagens")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class PersonagemRequest {

    @NotNull(message = "O campo nome está inválido")
    @NotBlank(message = "O campo nome está inválido")
    private String name;

    @NotNull(message = "O campo peso está inválido")
    @NotBlank(message = "O campo peso está inválido")
    private String height;

    @NotNull(message = "O campo massa está inválido")
    @NotBlank(message = "O campo massa está inválido")
    private String mass;

    @NotNull(message = "O campo cor do cabelo está inválido")
    @NotBlank(message = "O campo cor do cabelo está inválido")
    private String hairColor;

    @NotNull(message = "O campo Raça está inválido")
    @NotBlank(message = "O campo Raça está inválido")
    private String skinColor;

    @NotNull(message = "O campo Cor do Olho está inválido")
    @NotBlank(message = "O campo Cor do Olho está inválido")
    private String eyeColor;

    @NotNull(message = "O campo Ano de Nascimento está inválido")
    @NotBlank(message = "O campo Ano de Nascimento está inválido")
    private String birthYear;

    @NotNull(message = "O campo gendero está inválido")
    @NotBlank(message = "O campo gendero está inválido")
    private String gender;
}
