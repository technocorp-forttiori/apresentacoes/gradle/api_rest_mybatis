package com.apicomspringboot.demo.javaclient;


import com.apicomspringboot.demo.models.Person;
import com.apicomspringboot.demo.models.ResultsWrapper;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class JavaSpringClientTest {
    public List<Person> findPessoasIntegration(){
      RestTemplate pessoaTemplete = new RestTemplate();
      var listaPersonagens = pessoaTemplete.exchange("https://swapi.dev/api/people", HttpMethod.GET,
              null, ResultsWrapper.class);

      return listaPersonagens.getBody().getListPessoas();
    }
}
