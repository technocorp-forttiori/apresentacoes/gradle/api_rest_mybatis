package com.apicomspringboot.demo.models;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Table(name = "livros")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Livro {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long livroId;
    @NotNull(message = "Título não deve ser nulo")
    private String titulo;
    private String isbn;
    private String autor;
    private String editora;
    private Integer edicao;

}
