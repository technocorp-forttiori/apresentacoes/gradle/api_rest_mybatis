package com.apicomspringboot.demo.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Table (name = "personagens")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Personagem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long personagemId;
    private String name;
    private String height;
    private String mass;
    private String hair_color;
    private String skin_color;
    private String eye_color;
    private String birth_year;
    private String gender;
}
