package com.apicomspringboot.demo.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class ResultsWrapper {
    @JsonProperty("results")
    private List<Person> listPessoas;

    @JsonProperty("films")
    private List<String> listFilmes;
}
