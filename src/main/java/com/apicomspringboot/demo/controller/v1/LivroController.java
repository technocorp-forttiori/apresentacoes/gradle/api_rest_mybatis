package com.apicomspringboot.demo.controller.v1;

import com.apicomspringboot.demo.error.ResourceNotFoundException;
import com.apicomspringboot.demo.models.Livro;
import com.apicomspringboot.demo.service.LivroService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/livros")
@Api(value = "API REST de Livros")
@CrossOrigin(origins = "*")
public class LivroController {
    @Autowired
    LivroService livroService;

    @PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Salva um livro")
    public ResponseEntity<?> save(@RequestBody @Valid Livro livro) {
        try {
            livroService.save(livro);
            return ResponseEntity.status(HttpStatus.CREATED).body(livro);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ocorreu um erro inesperado");
        }
    }

    @PutMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Atualiza um livro no banco.")
    public ResponseEntity<?> update(@RequestBody @Valid Livro livro) {
        try {
            livroService.update(livro);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(livro);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ocorreu um erro inesperado");
        }
    }


    @DeleteMapping(path = "/delete/{livroId}")
    @ApiOperation(value = "Deleta um livro no banco.")
    public ResponseEntity<?> delete(@PathVariable(value = "livroId") @NotNull(message = "Id não pode ser nulo") @Valid Long livroId) {
        try {
            livroService.delete(livroId);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (ResourceNotFoundException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ocorreu um erro inesperado");
        }
    }


    @GetMapping(path = "/")
    @ApiOperation(value = "Retorna os livros do banco de dados.")
    public ResponseEntity<?> select() {
        try {
            List<Livro> livros = livroService.select();
            return ResponseEntity.status(HttpStatus.OK).body(livros);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ocorreu um erro inesperado");
        }
    }

    @GetMapping(path = "/{livroId}")
    @ApiOperation(value = "Retorna um livro do banco de dados.")
    public Livro retornaUmLivro(@PathVariable(value = "livroId") @NotNull Long livroId) {
        if (livroService.findById(livroId) != null) {
            return livroService.findById(livroId);
        } else {
            throw new ResourceNotFoundException("Livro não encontrado.");
        }
    }
}