package com.apicomspringboot.demo.controller.v1;

import com.apicomspringboot.demo.error.ResourceNotFoundException;
import com.apicomspringboot.demo.handler.RestExceptionHandler;
import com.apicomspringboot.demo.models.Personagem;
import com.apicomspringboot.demo.models.Person;
import com.apicomspringboot.demo.service.PersonagemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


@RestController
@RequestMapping(value = "/v1/personagem")
@Api(value = "API REST de Personagens")
@CrossOrigin(origins = "*")
@AllArgsConstructor
@Valid
public class PersonagemController {

    PersonagemService personagemService;

    @PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Salva um personagem")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Personagem> save(@RequestBody @Valid Personagem personagem) throws RestExceptionHandler {
        personagemService.save(personagem);
        return ResponseEntity.status(HttpStatus.CREATED).body(personagem);
    }

    @PutMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Atualiza um personagem no banco.")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<Personagem> update(@RequestBody @Valid Personagem personagem) {
            personagemService.update(personagem);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(personagem);
    }
//
//
//    @DeleteMapping(path = "/delete/{id}")
//    @ApiOperation(value = "Deleta um personagem no banco.")
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public HttpStatus delete(@PathVariable(value = "id")
//                                 @NotNull(message = "Id não pode ser nulo") @Valid Long id) throws RestExceptionHandler {
//        try {
//           personagemService.delete(id);
//        } catch (ResourceNotFoundException e) {
//            e.printStackTrace();
//            throw new RestExceptionHandler();
//        }
//        return HttpStatus.NO_CONTENT;
//    }
//
//
    @GetMapping(path = "/")
    @ApiOperation(value = "Retorna os personagens do banco de dados.")
    public ResponseEntity<?> select() {
        try {
            List<Person> personagens = personagemService.select();
            return ResponseEntity.status(HttpStatus.OK).body(personagens);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ocorreu um erro inesperado");
        }
    }

    @GetMapping(path = "/{personagemid}")
    @ApiOperation(value = "Retorna um personagem do banco de dados.")
    public Personagem retornaUmPersoangem(@PathVariable(value = "personagemid") @NotNull Long personagemid) {
        if (personagemService.findById(personagemid) != null) {
            return personagemService.findById(personagemid);
        } else {
            throw new ResourceNotFoundException("Personagem não encontrado.");
        }
    }
}
