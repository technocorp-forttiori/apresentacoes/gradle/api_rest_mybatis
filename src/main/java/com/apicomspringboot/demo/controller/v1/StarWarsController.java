package com.apicomspringboot.demo.controller.v1;

import com.apicomspringboot.demo.handler.RestExceptionHandler;
import com.apicomspringboot.demo.javaclient.JavaSpringClientTest;
import com.apicomspringboot.demo.service.PersonagemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1/swapi/saveAll")
@Api(value = "API REST de Personagens")
@CrossOrigin(origins = "*")
@AllArgsConstructor
public class StarWarsController {

    JavaSpringClientTest retornoPersonagens;

    PersonagemService personagemService;

    @GetMapping(path = "/")
    @ApiOperation(value = "Retorna os personagens do banco de dados.")
    public ResponseEntity<Void> select() throws RestExceptionHandler {
            personagemService.saveSwapi();
            return ResponseEntity.status(HttpStatus.OK).build();
    }


}
