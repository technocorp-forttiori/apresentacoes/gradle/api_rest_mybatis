package com.apicomspringboot.demo.repository;


import com.apicomspringboot.demo.models.Personagem;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface PersonagemRepository {

    @Update("UPDATE personagens SET name=#{name},height=#{height},"
            + "mass=#{mass},hair_color=#{hair_color},skin_color=#{skin_color},eye_color=#{eye_color}" +
            ",birth_year=#{birth_year},gender=#{gender} WHERE personagemId=#{personagemId}")
    public int update(Personagem personagem);

    @Select("SELECT personagemid AS personagemid,name,height,mass,hair_color,skin_color,eye_color,birth_year,gender FROM personagens")
    public List<Personagem> select();

    @Delete("DELETE FROM personagens WHERE personagemid=#{personagemid}")
    public Long delete(Long id);

    @Select("SELECT * FROM personagens WHERE personagemid=#{personagemid}")
    public Optional<Personagem> findById(Long id);

    @Insert("INSERT INTO personagens " +
            "(name,height,mass,hair_color,skin_color,eye_color,birth_year,gender) " +
            "VALUES " +
            "(#{personagem.name}," +
            "#{personagem.height}," +
            "#{personagem.mass}," +
            "#{personagem.hair_color}," +
            "#{personagem.skin_color}," +
            "#{personagem.eye_color}," +
            "#{personagem.birth_year}," +
            "#{personagem.gender})")
    @Options(keyColumn = "personagemid", keyProperty = "personagem.personagemId", useGeneratedKeys = true)
    public Integer insert(@Param("personagem") Personagem personagem);
}
