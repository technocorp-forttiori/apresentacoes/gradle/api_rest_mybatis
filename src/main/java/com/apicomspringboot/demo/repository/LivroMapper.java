package com.apicomspringboot.demo.repository;

import com.apicomspringboot.demo.models.Livro;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface LivroMapper {

    @Update("UPDATE livros SET titulo=#{titulo},isbn=#{isbn},"
            + "autor=#{autor},edicao=#{edicao},editora=#{editora} WHERE livroId=#{livroId}")
    public int update(Livro livro);

    @Select("SELECT livroId AS livroId,titulo,isbn,autor,edicao,editora FROM livros")
    public List<Livro> select();

    @Delete("DELETE FROM livros WHERE livroId=#{livroId}")
    public Long delete(Long livroId);

    @Select("SELECT * FROM livros WHERE livroId=#{livroId}")
    public Livro findById(Long livroId);

    @Insert("INSERT INTO livros " +
            "(titulo,isbn,autor,edicao,editora) " +
            "VALUES " +
            "(#{livro.titulo},#{livro.isbn},#{livro.autor},#{livro.edicao},#{livro.editora})")
    @Options(keyColumn = "livroid", keyProperty = "livro.livroId", useGeneratedKeys = true)
    public Integer insert(@Param("livro") Livro livro);
}