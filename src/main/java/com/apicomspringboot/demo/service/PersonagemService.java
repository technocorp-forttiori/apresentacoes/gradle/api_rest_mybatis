package com.apicomspringboot.demo.service;

import com.apicomspringboot.demo.error.ResourceNotFoundException;
import com.apicomspringboot.demo.handler.RestExceptionHandler;
import com.apicomspringboot.demo.javaclient.JavaSpringClientTest;
import com.apicomspringboot.demo.models.Personagem;
import com.apicomspringboot.demo.models.Person;
import com.apicomspringboot.demo.repository.PersonagemRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PersonagemService {

    JavaSpringClientTest javaSpringClientTest;

    PersonagemRepository personagemRepository;

    public List<Person> findPersonagemService() {
        var resultados = javaSpringClientTest.findPessoasIntegration();
        return resultados;
    }

    public void saveSwapi() throws RestExceptionHandler {
        var listaPersonagens = javaSpringClientTest.findPessoasIntegration().stream().map(personagem -> {
            return Personagem.builder()
                    .name(personagem.getName())
                    .height(personagem.getHeight())
                    .mass(personagem.getMass())
                    .hair_color(personagem.getHair_color())
                    .skin_color(personagem.getSkin_color())
                    .eye_color(personagem.getEye_color())
                    .birth_year(personagem.getBirth_year())
                    .gender(personagem.getGender())
                    .build();
        }).collect(Collectors.toList());
        listaPersonagens.forEach(p -> {
            personagemRepository.insert(p);
        });
    }

    public void update(Personagem personagem) throws ResourceNotFoundException {
        this.personagemRepository.findById(personagem.getPersonagemId()).orElseThrow(()
                -> new ResourceNotFoundException("Personagem não encontrado"));
        personagemRepository.update(personagem);
    }

    public void delete(Long id) {
        this.personagemRepository.delete(id);
    }

    public void save(Personagem personagem) {
        this.personagemRepository.insert(personagem);
    }

    public List<Person> select() {
        List<Person> personagens = findPersonagemService();
                this.personagemRepository.select();
        return personagens;
    }

    public Personagem findById(Long id) {
        return this.personagemRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Personagem não encontrado"));
    }
}
